﻿using System;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace SerialPortTrans2
{
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        static void Write(SerialPortConfigSection v)
        {
            Console.WriteLine(Resource.SerialPortInfoFormat, v.PortName, v.BaudRate, v.Parity, v.DataBits, v.StopBits);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        static void Exit(string v)
        {
            Console.WriteLine(v);
            Console.Write(Resource.AppErrorExitMessage);
            Console.Read();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        static void Exit(Exception e)
        {
            while (e != null)
            {
                Console.Write(e.ToString());
                Console.WriteLine(e.StackTrace);
                e = e.InnerException;
            }
            Console.Write(Resource.AppErrorExitMessage);
            Console.Read();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                var serialPort1 = System.Configuration.ConfigurationManager.GetSection("serialPortTrans2/serialPort1") as SerialPortConfigSection;
                if (serialPort1 != null)
                {
                    Write(serialPort1);
                }
                else
                {
                    Exit(Resource.SerialPort1ConfigErrorMessage);
                    return;
                }

                var serialPort2 = System.Configuration.ConfigurationManager.GetSection("serialPortTrans2/serialPort2") as SerialPortConfigSection;
                if (serialPort2 != null)
                {
                    Write(serialPort2);
                }
                else
                {
                    Exit(Resource.SerialPort2ConfigErrorMessage);
                    return;
                }

                //显示标题
                Console.Title = string.Format(Resource.AppTitleFormat, serialPort1.PortName, serialPort2.PortName);

                using (var serviceRoutine = new ServiceRoutine(serialPort1, serialPort2))
                {
                    serviceRoutine.DataReceived += serviceRoutine_DataReceived;
                    serviceRoutine.SendErrorOccurred += serviceRoutine_SendErrorOccurred;
                    serviceRoutine.Start();

                    while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                    {
                        Thread.Yield();
                    }

                    serviceRoutine.Close();
                }
            }
            catch (Exception ex)
            {
                Exit(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        static string ToHex(byte[] bytes, int count)
        {
            StringBuilder sb = new StringBuilder(count * 3);
            for (int i = 0; i < count; ++i)
            {
                sb.Append(bytes[i].ToString("X2"));
                sb.Append(' ');
            }
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="servceiRoutine"></param>
        /// <param name="serialPort"></param>
        /// <param name="dataArray"></param>
        /// <param name="dataCount"></param>
        static void serviceRoutine_DataReceived(ServiceRoutine servceiRoutine, SerialPort serialPort, byte[] dataArray, int dataCount)
        {
            Console.WriteLine(Resource.DataReceivedMessageFormat, serialPort.PortName, ToHex(dataArray, dataCount));
        }
        /// <summary>
        /// 
        /// </summary>
        /// 
        /// <param name="servceiRoutine"></param>
        /// <param name="serialPort"></param>
        /// <param name="dataArray"></param>
        /// <param name="dataCount"></param>
        /// <param name="exception"></param>
        static void serviceRoutine_SendErrorOccurred(ServiceRoutine servceiRoutine, SerialPort serialPort, byte[] dataArray, int dataCount, Exception exception)
        {
            Console.WriteLine(Resource.SendErrorOccurredMessageFormat, serialPort.PortName, ToHex(dataArray, dataCount), exception.ToString());
        }
    }
}